# Vorlage für Abschlussarbeiten an der HS Kempten

Diese Vorlage sollte alle formalen Anforderungen an Abschlussarbeiten an der Hochschule Kempten erfüllen. Die Vorlage besteht aus mehreren Dateien, am wichtigsten ist die `main.tex`
Fehlerkorrekturen und Verbesserungen sind via PR gern gesehen und werden (hoffentlich) zeitnah gemerged. Solltest du kein Bitbucket haben, dann kannst du deine Changes auch an dbx12 at dbx12 dot de senden und sie werden mit Attribution eingepflegt.


## main.tex
Diese Datei enthält die eigentliche Arbeit. Der Befehl `pdflatex` muss mit dieser Datei ausgeführt werden, für die anderen Dateien ist das nicht erforderlich.
In dieser Datei sind auch die Einstellungen für die Arbeit zu setzen (in der Datei ist der Bereich mit `Setup` markiert).

### Darstellung von Links
Bei der Darstellung von Links in der Arbeit scheiden sich die Geister, in einer PDF Datei ist der Link in blau sofort als klickbar erkennbar, in der gedruckten Form wirkt es eher verwirrend. Daher ist die Darstellung von Links im Dokument konfigurierbar, (in der `main.tex` ist der Bereich mit `Einstellungen für Links` markiert). Es ist zu bemerken, dass die Optionen `hideLinks` und `colorLinks = true` sich gegenseitig ausschließen, es dar nur eine der beiden Optionen gesetzt sein. `hideLinks` deaktiviert die Hervorhebung von Links völlig, was sich gut für die Druckversion eignet. `colorLinks = true` versteckt die Boxen um Links und färbt den Text stattdessen ein (die Farbkonfiguration ist direkt unter dieser Option in `main.tex`).

## commands.tex
Hier sind alle Befehle definiert, die das Arbeit-Schreiben leichter machen sollen, das sind vor allem die Befehle `\image`, `\figref` und `\figrefp` (siehe Abschnitt Befehle weiter unten)

## declaration.tex
In dieser Datei steht die Erklärung, dass man die Arbeit selbstständig angefertigt hat und ggf. die Ermächtigung, die Kurzzusammenfassung zu veröffentlichen.
Die Ermächtigung war bisher freiwillig, wenn man diese nicht geben möchte, dann muss die zweite Section gelöscht werden (in der Datei markiert).

## restriction_notice.tex
Diese Datei enthält den Sperrvermerk. Dieser ist mit der Firma, in der die Arbeit erstellt wurde, abzusprechen. Der Text wird automatisch mit den Werten aus der `main.tex` ausgefüllt und müsste theoretisch nicht bearbeitet werden.

Wird er nicht benötigt, dann muss der `\input` Befehl in der `main.tex` gelöscht werden. 

## titlepage.tex
Die Titelseite der Arbeit. Auch diese wird automatisch mit den Werten aus der `main.tex` befüllt. Eventuell muss die "durchgeführt bei" Zeile gelöscht werden, wenn an der Hochschule bearbeitet wurde. Um den Platzhalter für das Firmenlogo auszublenden, kann die Datei `img/logo-blank.jpg` in `img/logo-company.jpg` umbenannt werden oder die `titlepage.tex` entsprechend angepasst werden. Der Platzhalter für das Firmenlogo ist auf 1500x480px bei 300dpi dimensioniert, kann natürlich aber angepasst werden.

# Befehle

## \image

Der `\image` Befehl fügt eine Grafik ein und setzt eine Beschreibung darunter. Außerdem wird ein Label definiert, auf das man referenzieren kann.

Beispiel: 

```
\image{katzen.png}{Eine gewöhnliche Hauskatze}{house-cat}
```

Dieser Befehl fügt das Bild `katzen.png` aus dem Ordner `img` ein, beschreibt es mit "Abbildung 1: Eine gewöhnliche Hauskatze" und erstellt das Label `house-cat`. Auf das Label kann man mit den folgenden Befehlen referenzieren.

**Überschreiben der Bildbreite** Standardmäßig wird die Grafik mit 90% der Textbreite eingefügt, wird ein weiterer Parameter zu Beginn übergeben, so wird dieser als Prozent der Textbreite interpretiert.

Beispiel:

```
\image[0.7]{katzen.png}{Eine gewöhnliche Hauskatze}{house-cat}
```

Dieser Befehl fügt das Bild wie zuvor beschrieben ein, allerdings nun nur noch mit 70% der Textbreite anstatt von standardmäßig 90% Textbreite.

## \figref

Mit diesem Befehl bezieht man sich auf ein eingefügtes Bild.

Beispiel:

```
Hauskatzen (in \figref{house-cat} zu sehen) leben in Häusern.
```

Ausgabe:

```
Hauskatzen (in Abbildung 1 zu sehen) leben in Häusern.
```

## \figrefp

Mit diesem Befehl bezieht man sich auf ein eingefügtes Bild und gibt zusätzlich die Seite, auf der sich das Bild befindet an.

Beispiel:

```
Hauskatzen (in \figrefp{house-cat} zu sehen) leben in Häusern.
```

Ausgabe:

```
Hauskatzen (in Abbildung 1 (S. 14) zu sehen) leben in Häusern.
```


## \blockquote

Mit diesem Befehl wird ein Absatz eingefügt und als Zitat markiert. Der erste Parmeter ist der zu zitierende Text, der Zweite die Quelle und der Dritte entweder leer oder die Seite, auf der der Text in der Quelle steht.

Beispiel: 

```
\blockquote{The cake is a lie.}{GLaDOS_Manual}{}

vs.

\blockquote{The cake is a lie.}{GLaDOS_Manual}{p. 42}
```

Ausgabe:

```
"The cake is a lie." [1]

vs.

"The cake is a lie." [1, p. 42]
```
